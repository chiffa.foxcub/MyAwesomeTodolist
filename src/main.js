var app = new Vue({
    el: '#app',
    data: {
        items: sessionStorage.taskStore?JSON.parse(sessionStorage.taskStore):[],
        dialog: false,
        CurrentTask: [],
        id: this.id ? this.id:'',
        taskName: this.taskName ? this.taskName:'',
        description: this. description ? this.description:'',
        order: -1
    },
    mounted: function () {
        //sort by id on page load
        this.$nextTick(function () {
            var compare =  function compare(a,b) {
                if (a.id < b.id)
                    return -1;
                if (a.id > b.id)
                    return 1;
                return 0;
            };
            this.items.sort(compare);
        });
    },
    methods: {
        updateStore(store){
           sessionStorage.setItem("taskStore", JSON.stringify(store));
        },

        getColor: function (req) {
            return req.isDone?'green':'';
        },

        createTask(){
            this.id = '';
            this.taskName = '';
            this.description = '';
            this.dialog = true;
        },

        addTask(){
            var id;
            var taskName = this.taskName;
            var description = this.description;
            var store = this.items;
            if(!this.id) {
                id = this.items.length;
                store.push({"id": id, "taskName": taskName, "description": description,"isDone": false});
            } else {
                for (var i in store) {
                    if (store[i].id === this.id) {
                        store[i].taskName = this.taskName;
                        store[i].description = this.description;
                        break;
                    }
                }
            }

            this.updateStore(store);
            this.dialog = false;
        },

        editTask(req){
            var store = this.items;
            for (var i in store) {
                if (store[i].id === req.id) {
                    this.id = req.id;
                    this.taskName = req.taskName;
                    this.description = req.description;
                    this.dialog = true;
                    break;
                }
            }
        },

        deleteTask(req){
            var store = this.items;
            var newstore = store.filter(function(el) {
                return el.id !== req.id;
            });
            this.updateStore(newstore);
            this.items = sessionStorage.taskStore?JSON.parse(sessionStorage.taskStore):[];
        },

        markDone(req){
            var store = this.items;
            for (var i in store) {
                if (store[i].id === req.id) {
                    store[i].isDone = true;
                    break;
                }
            }
            this.updateStore(store);
        },

        sortTasks(){
            var compare =  function compare(a,b) {
                if (a.taskName < b.taskName)
                    return -1;
                if (a.taskName > b.taskName)
                    return 1;
                return 0;
            };

            this.items.sort(compare);
            
            //for reverse sort
            if(this.order>0){
                this.items.reverse();
            }

            this.order = this.order * -1;
        },

    }
});